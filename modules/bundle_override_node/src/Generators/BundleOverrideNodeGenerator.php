<?php

namespace Drupal\bundle_override_node\Generators;

use Drupal\Core\Cache\Cache;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Entity\EntityTypeBundleInfo;

/**
 * Class BundleOverrideNodeGenerator.
 *
 * @package Drupal\bundle_override_node\Generators
 */
class BundleOverrideNodeGenerator extends BaseGenerator {

  /**
   * Field Module Name.
   *
   * @const string
   */
  const FIELD_MODULE_NAME = 'machine_name';

  /**
   * Field Bundle Name.
   *
   * @const string
   */
  const FIELD_BUNDLE_NAME = 'bundle_name';

  /**
   * Name.
   *
   * @var string
   */
  protected $name = 'bundle-override-node';

  /**
   * Description.
   *
   * @var string
   */
  protected $description = 'Generates a bundle_override_node plugin.';

  /**
   * Alias.
   *
   * @var string
   */
  protected $alias = 'bo-node-plugin';

  /**
   * The template path.
   *
   * @var string
   */
  protected $templatePath = __DIR__;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected $bundleInfo;

  /**
   * BundleOverrideNodeGenerator constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandler|null $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo|null $bundleInfo
   *   THe bundle info.
   * @param string $name
   *   The name.
   */
  public function __construct(ModuleHandler $moduleHandler = NULL, EntityTypeBundleInfo $bundleInfo = NULL, $name = NULL) {
    parent::__construct($name);
    $this->moduleHandler = $moduleHandler;
    $this->bundleInfo = $bundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    // Module machine name.
    $questions = [];
    $questions[static::FIELD_MODULE_NAME] = new Question('Module machine name ?');
    $questions[static::FIELD_MODULE_NAME]->setValidator([
      Utils::class,
      'validateMachineName',
    ]);

    // Bundle.
    $questions[static::FIELD_BUNDLE_NAME] = new Question('Node bundle ?');
    $questions[static::FIELD_BUNDLE_NAME]->setValidator([
      $this,
      'validateBundle',
    ]);
    $this->collectVars($input, $output, $questions);

    // Génération des templates.
    $this->generateTemplates();
  }

  /**
   * Génération des templates.
   */
  protected function generateTemplates() {
    $className = Utils::camelize($this->vars[static::FIELD_BUNDLE_NAME]) . 'Node';
    $this->vars['class_name'] = $className;

    // Création du plugin.
    $this->addFile()
      ->path('src/Plugin/bundle_override/Objects/node/' . $className . '.php')
      ->template('templates/bundle-override-node-plugin.twig');

    // Clear cache.
    foreach (Cache::getBins() as $bin) {
      $bin->deleteAll();
    }
  }

  /**
   * Validate bundle.
   *
   * @param string $value
   *   The value.
   */
  public function validateBundle($value) {
    // Get Node Bundles.
    $bundles = $this->bundleInfo->getBundleInfo('node');
    if (!array_key_exists($value, $bundles)) {
      throw new \UnexpectedValueException('The value is not correct bundle.');
    }
    return $value;
  }

}
