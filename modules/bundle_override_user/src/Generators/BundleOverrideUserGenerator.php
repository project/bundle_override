<?php

namespace Drupal\bundle_override_user\Generators;

use Drupal\Core\Cache\Cache;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Entity\EntityTypeBundleInfo;

/**
 * Class BundleOverrideUserGenerator.
 *
 * @package Drupal\bundle_override_node\Generators
 */
class BundleOverrideUserGenerator extends BaseGenerator {

  /**
   * Field Module Name.
   *
   * @const string
   */
  const FIELD_MODULE_NAME = 'machine_name';

  /**
   * Field Bundle Name.
   *
   * @const string
   */
  const FIELD_BUNDLE_NAME = 'bundle_name';

  /**
   * Name.
   *
   * @var string
   */
  protected $name = 'bundle-override-user';

  /**
   * Description.
   *
   * @var string
   */
  protected $description = 'Generates a bundle_override_user plugin.';

  /**
   * Alias.
   *
   * @var string
   */
  protected $alias = 'bo-user-plugin';

  /**
   * The template path.
   *
   * @var string
   */
  protected $templatePath = __DIR__;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected $bundleInfo;

  /**
   * BundleOverrideUserGenerator constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandler|null $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo|null $bundleInfo
   *   The bundleInfo.
   * @param string $name
   *   The name.
   */
  public function __construct(ModuleHandler $moduleHandler = NULL, EntityTypeBundleInfo $bundleInfo = NULL, $name = NULL) {
    parent::__construct($name);
    $this->moduleHandler = $moduleHandler;
    $this->bundleInfo = $bundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    // Module machine name.
    $questions = [];
    $questions[static::FIELD_MODULE_NAME] = new Question('Module machine name ?');
    $questions[static::FIELD_MODULE_NAME]->setValidator([
      Utils::class,
      'validateMachineName',
    ]);

    // Bundle.
    $questions[static::FIELD_BUNDLE_NAME] = new Question('User bundle ?');
    $questions[static::FIELD_BUNDLE_NAME]->setValidator([
      $this,
      'validateBundle',
    ]);
    $this->collectVars($input, $output, $questions);

    // Génération des templates.
    $this->generateTemplates();
  }

  /**
   * Génération des templates.
   */
  protected function generateTemplates() {
    $className = Utils::camelize($this->vars[static::FIELD_BUNDLE_NAME]) . 'User';
    $this->vars['class_name'] = $className;

    // Création du plugin.
    $this->addFile()
      ->path('src/Plugin/bundle_override/Objects/user/' . $className . '.php')
      ->template('templates/bundle-override-user-plugin.twig');

    // Clear cache.
    foreach (Cache::getBins() as $bin) {
      $bin->deleteAll();
    }
  }

  /**
   * Validate bundle.
   *
   * @param string $value
   *   The value.
   */
  public function validateBundle($value) {
    // Get Users Bundles.
    $bundles = $this->bundleInfo->getBundleInfo('user');
    if (!array_key_exists($value, $bundles)) {
      throw new \UnexpectedValueException('The value is not correct bundle.');
    }
    return $value;
  }

}
