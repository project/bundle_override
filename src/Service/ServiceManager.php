<?php

namespace Drupal\bundle_override\Service;

use Drupal\bundle_override\Manager\EntityTypes\BundleOverrideEntityTypesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ServiceManager.
 *
 * @package Drupal\bundle_override\Service
 */
class ServiceManager {

  /**
   * Nom du service.
   *
   * @const string
   */
  const SERVICE_NAME = 'bundle_override.service_manager';

  /**
   * Container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * ServiceManager constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Add service to storage.
   *
   * @param \Drupal\bundle_override\Manager\EntityTypes\BundleOverrideEntityTypesInterface $instance
   *   Add the service to the container.
   */
  public function addService(BundleOverrideEntityTypesInterface $instance) {
    if (!$this->container->has($instance->getServiceId())) {
      $this->container->set($instance->getServiceId(), $instance);
    }
  }

}
