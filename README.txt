CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Use
 * Maintainers
 
 
INTRODUCTION
------------
Bundle override allows you to use specific class for entity bundle.
As an example, if you have a bundle of node called 'article' and you want
to add specific methods for this bundle (at load, save, delete or whatever)
you can define a specific class (as a Plugin) and this class will be used
instead of default \Drupal\node\Entity\Node.

Then you can use Article::loadMultiple() to load only 'article' bundled
nodes, and other loading function limited to the entity bundle.

This approach can bring some features like adding process on entity save,
without using hook or event definition and then bundle filter.


By default, bundle_override brings plugin for node and taxonomy_term entity
but you can add any entity type plugin to use the bundle class.
You can see the node and taxonomy_term plugins to have examples.


REQUIREMENTS
------------

This module require
 * "php": ">=7.0"

RECOMMENDED MODULES
------------
bundle_override comes with several submodules that helps you
define bundle for commons entities like node and terms.
 * bundle_override_node
 * bundle_override_term


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
------------

  * No specific configuration is needed.


USE
------------
For example, if you want to define a class for the node bundle 'article'

  * Enable bundle_override_node
    `drush en bundle_override_node`
  * Launch drush generator command :
    `drush generate bo-node-plugin`
  * Choose which module will host the plugin
  * Indicates the bundle name


MAINTAINERS
-----------
tsecher - https://www.drupal.org/u/tsecher
